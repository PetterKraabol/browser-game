<?php
include_once($_SERVER['DOCUMENT_ROOT'].'/process.php');

//If the user is logged out, display login.php instead
if(!$isLogin){
	include($document_root.'/login.php');
}else if($isLogin && $user->data['stage'] < 2){
	include($document_root.'/stage.php');
}else{
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<title>Title goes here at some point</title>
		<link rel="stylesheet" href="/css/style.css?ts=<?php echo time(); ?>"/>
		<link rel="stylesheet" href="/css/sidrdark.css"/>
		<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
		<!--[if ie]><meta http-equiv="X-UA-Compatible" content="IE=edge"/><![endif]-->
	</head>
	<body>
<?php require_once('./template/top.php'); ?>
		<div id="dialog"></div>
		<div id="dialog-blur"></div>
		<div id="sidr">
			<form id="live-search" action="" class="styled" method="post">
				<input type="text" class="text-input" id="village-search-filter" value=""/>
			</form>
			<ul class="village-list"></ul>
			<a class="create-new-village" href="#" title="new village">+</a>
		</div>
		<div class="container">
			<div id="content" class="fade-in">
				<header>
					<h1 class="village-name">Loading...</h1>
					<ul id="stats">
						<li>
							<a href="#" title="Money"><span class="village-coins">0</span> coins</a>
						</li>
						<li>
							<a href="#" title="Food"><span class="village-food">0</span> food</a>
						</li>
						<li>
							<a href="#" title="Wood"><span class="village-wood">0</span> wood</a>
						</li>
						<li>
							<a href="#" title="Wood"><span class="village-stone">0</span> stone</a>
						</li>
						<li>
							<a href="#" title="Villagers"><span class="village-villagers">0</span> villagers</a>
						</li>
						<li>
							<a href="#" title="Health"><span class="village-health">0</span> health</a>
						</li>
					</ul>
					<div class="clear-both"></div>
					<a title="24,379 Exp"><div id="experience-bar"><div id="experience-bar-progress" style="width:0px;"></div></div></a>
				</header>
				<div class="village-description-container fade-in one"><div class="village-description"></div></div>
				<span class="toggle-village-description clickable">Description</span>
				<div id="village-work-container" class="fade-in one">
					<div style="float:left;"><!-- -->
						
					</div>
					<div style="float:left; width:50%;"><!-- -->
						
					</div>
				</div>
				<a href="#" class="awesomebutton">Awesome Button</a>
				<p class="test-output"></p>
				<?php echo $_GET['username'].'/'.$_GET['village'].'/'.$_GET['work']; ?> 
				<br>
				Read the announcement at <a href="http://petter.kraabol.com/2013/05/new-project-browser-game/" target="_blank">Petter.Kraabol.com</a>
				<div class="clear-both"></div>
				<footer class="fade-in three">
					<a target="_blank" href="http://petter.kraabol.com">&copy; Petter Kraabøl 2013 - Early development</a>
				</footer>
			</div>
			<!--<a href="./oauth/redirect.php"><img src="./oauth/images/darker.png" alt="Sign in with Twitter"/></a>-->
			<div class="clear-both"></div>
		</div>
		<script>
			var selectedUser = <?php echo $selectedUser->data['user_id']; ?>;
			var currentVillageID = <?php echo $selectedVillage->data['village_id']; ?>;
		</script>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
		<script src="/js/jquery.sidr.min.js"></script>
		<script src="/js/jquery.hotkeys.js"></script>
		<script src="/js/script.js"></script>
	</body>
</html>

<?php
}
?>