<?php
	session_start();
	
	$document_root = $_SERVER['DOCUMENT_ROOT'];
	require_once($document_root.'/config.php');

	class Core {
		public $dbh; // handle of the db connection
		private static $instance;

		private function __construct()  {

			// building data source name from config
			$dsn = 'mysql:host=' . Config::read('db.host') . ';dbname=' . Config::read('db.basename') . ';port=' . Config::read('db.port') .';connect_timeout=15';

			// getting DB user from config
			$user = Config::read('db.user');

			// getting DB password from config
			$password = Config::read('db.password');

			$this->dbh = new PDO($dsn, $user, $password);
			$this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		}

		public static function getInstance() {
			if (!isset(self::$instance)) {
				$object = __CLASS__;
				self::$instance = new $object;
			}
			return self::$instance;
		}
	}
	
	class User {
		
		//Login user and register if not already registered
		public function login($_social_id, $_login_code, $_provider, $_email){
			$returnValue=false;
			$query='SELECT * FROM users WHERE social_id = :social_id AND provider = :provider';
			
			try {
				$pdoCore = Core::getInstance();
				$pdoObject = $pdoCore->dbh->prepare($query);
				$queryArray = array(':social_id'=>$_social_id, ':provider'=>$_provider);
				
				if ($pdoObject->execute($queryArray)) {
					$pdoObject->setFetchMode(PDO::FETCH_ASSOC);;
					
					//Login
					if($pdoObject->rowCount() > 0){
						while ($dataRow = $pdoObject->fetch()) {
							$_user_id=$dataRow;
						}
						$this->updateLoginCode($_login_code, $_user_id['user_id']);
						
					}else{
						$this->register($_social_id, $_login_code, $_provider, $_email);
					}
					$returnValue=true;
				}
			}
			catch(PDOException $pe) {
				trigger_error('Database error. ' . $pe->getMessage() , E_USER_ERROR);
			}
			return $returnValue;
		}
		
		//Register a new user with social id, login code and provider.
		public function register($_social_id, $_login_code, $_provider, $_email){
			$returnValue=false;
			$_username = ''.rand(1,999999999999).'';
			$query='INSERT INTO users (social_id, login_code, provider, username, email, register_date) VALUES (:social_id, :login_code, :provider, :username, :email, now())';
			
			try {
				$pdoCore = Core::getInstance();
				$pdoObject = $pdoCore->dbh->prepare($query);
				$queryArray = array(':social_id'=>$_social_id, ':login_code'=>$_login_code, ':provider'=>$_provider, ':username'=>$_username, ':email'=>$_email);
				
				if ($pdoObject->execute($queryArray)) {
					
					//If registration is complete, login
					$this->login($_social_id, $_login_code, $_provider, $_email);
					$returnValue=true;
				}
			}
			catch(PDOException $pe) {
				trigger_error('Database error. ' . $pe->getMessage() , E_USER_ERROR);
			}
			return $returnValue;
		}
		
		//Update login code which is used to validate is the user is logged in.
		public function updateLoginCode($_login_code, $_user_id){
			$returnValue=false;
			$query='UPDATE users SET login_code = :login_code WHERE user_id = :user_id';
			
			try {
				$pdoCore = Core::getInstance();
				$pdoObject = $pdoCore->dbh->prepare($query);
				$queryArray = array(':login_code'=>$_login_code, ':user_id'=>$_user_id);
				
				if ($pdoObject->execute($queryArray)) {
					$_SESSION['login_code']=$_login_code;
					$_SESSION['user_id']=$_user_id;
					
					//If update complete, refresh
					header('location: /');
					$returnValue=true;
				}
			}
			catch(PDOException $pe) {
				trigger_error('Database error. ' . $pe->getMessage() , E_USER_ERROR);
			}
			return $returnValue;
		}
		
		//Update username and email
		public function updateUsernameAndEmail($_username, $_email, $_user_id, $_stage){
			$returnValue=false;
			
			if($_stage == 0){
				$query='UPDATE users SET username = :username, email = :email, stage = 1 WHERE user_id = :user_id';
			}else{
				$query='UPDATE users SET username = :username, email = :email WHERE user_id = :user_id';
			}
			
			try {
				$pdoCore = Core::getInstance();
				$pdoObject = $pdoCore->dbh->prepare($query);
				$queryArray = array(':username'=>$_username, 'email'=>$_email, ':user_id'=>$_user_id);
				
				if ($pdoObject->execute($queryArray)) {
					header('location: /');
					$returnValue=true;
				}
			}
			catch(PDOException $pe) {
				trigger_error('Database error. ' . $pe->getMessage() , E_USER_ERROR);
			}
			return $returnValue;
		}
		
		//Get user info
		public function getUserBy($_by, $_value) {
			$_by = htmlentities($_by);
			$returnValue=false;
			if ($_by == 'user_id'){
				$query='SELECT * FROM users WHERE user_id = :value';
			}else if($_by == 'username'){
				$query='SELECT * FROM users WHERE username = :value';
			}else if($_by == 'social_id'){
			$query='SELECT * FROM users WHERE social_id = :value';
			}
			
			try {
				$pdoCore = Core::getInstance();
				$pdoObject = $pdoCore->dbh->prepare($query);
				$queryArray = array(':value'=>$_value);
				
				if ($pdoObject->execute($queryArray)) {
					$pdoObject->setFetchMode(PDO::FETCH_ASSOC);;
					while ($dataRow = $pdoObject->fetch()) {
						$this->data=$dataRow;
					}
					$returnValue=true;
				}
			}
			catch(PDOException $pe) {
				trigger_error('Database error. ' . $pe->getMessage() , E_USER_ERROR);
			}
			return $returnValue;
		}
	}

	class Village {
		
		//Create a new village
		public function newVillage($_user, $_name, $_description, $_x, $_y, $_stage) {
			
			$query = 'SELECT count(*) FROM villages WHERE x = :x AND y = :y';
			try {
				$pdoCore = Core::getInstance();
				$pdoObject = $pdoCore->dbh->prepare($query);
				$queryArray = array(':x'=>$_x, ':y'=>$_y);
				
				if ($pdoObject->execute($queryArray)) {
					$number_of_rows = $pdoObject->fetchColumn();
					if($number_of_rows == 0){
						
						//Check if name is taken by user
						$query = 'SELECT count(*) FROM villages WHERE name = :name AND user = :user';
						try {
							$pdoCore = Core::getInstance();
							$pdoObject = $pdoCore->dbh->prepare($query);
							$queryArray = array(':name'=>$_name, ':user'=>$_user);

							if ($pdoObject->execute($queryArray)) {
								$number_of_rows = $pdoObject->fetchColumn();
								if($number_of_rows == 0){
									//If everything is OK, register village
									$query='INSERT INTO villages (user, name, x, y, description, create_date, health, max_health)
											VALUES (:user, :name, :x, :y, :description, now(), 100, 100)';

									try {
										$pdoCore = Core::getInstance();
										$pdoObject = $pdoCore->dbh->prepare($query);
										$queryArray = array(':user'=>$_user, ':name'=>$_name, ':x'=>$_x, ':y'=>$_y, ':description'=>$_description);
										if($pdoObject->execute($queryArray)){
											
											//Check if this is the first village, then set this as default.
											if($_stage == 1){
												$query='SELECT * FROM villages WHERE user = :user';
												try {
													$pdoCore = Core::getInstance();
													$pdoObject = $pdoCore->dbh->prepare($query);
													$queryArray = array(':user'=>$_user);

													if ($pdoObject->execute($queryArray)) {
														$row = $pdoObject->fetch(PDO::FETCH_ASSOC);
														$village_id = $row['village_id'];
														
														//Set default village for user
														$query='UPDATE users SET default_village = :village_id, stage = 2 WHERE user_id = :user';
														try {
															$pdoCore = Core::getInstance();
															$pdoObject = $pdoCore->dbh->prepare($query);
															$queryArray = array(':village_id'=>$village_id, ':user'=>$_user);
															$pdoObject->execute($queryArray);
															
															//Everything is OK, refresh website.
															header('location: /');
														}
														catch(PDOException $pe) {
															trigger_error('Database error. ' . $pe->getMessage() , E_USER_ERROR);
														}
													}
												}
												catch(PDOException $pe) {
													trigger_error('Database error. ' . $pe->getMessage() , E_USER_ERROR);
												}
											}
											
										}
									}
									catch(PDOException $pe) {
										trigger_error('Database error. ' . $pe->getMessage() , E_USER_ERROR);
									}

								}else{
									$error = 'you already have a village with that name.';
								}
							}
						}
						catch(PDOException $pe) {
							trigger_error('Database error. ' . $pe->getMessage() , E_USER_ERROR);
						}
						
					}else{
						$error = 'there\'s another village with these coordinates.';
					}
				}
			}
			catch(PDOException $pe) {
				trigger_error('Database error. ' . $pe->getMessage() , E_USER_ERROR);
			}
		}
		
		//Get village information
		public function getVillageBy($_by, $_value, $_userid) {
			$_by = htmlentities($_by);
			$returnValue=false;
			if ($_by == 'id'){
				$query='SELECT * FROM villages WHERE village_id = :value AND user = :user';
			}else if($_by == 'name'){
				$query='SELECT * FROM villages WHERE name = :value AND user = :user';
			}
			
			try {
				$pdoCore = Core::getInstance();
				$pdoObject = $pdoCore->dbh->prepare($query);
				$queryArray = array(':value'=>$_value, ':user'=>$_userid);
				
				if ($pdoObject->execute($queryArray)) {
					$pdoObject->setFetchMode(PDO::FETCH_ASSOC);;
					while ($villageDataRow = $pdoObject->fetch()) {
						$this->data=$villageDataRow;
					}
					$returnValue=true;
				}
			}
			catch(PDOException $pe) {
				trigger_error('Could not connect to MySQL database. ' . $pe->getMessage() , E_USER_ERROR);
			}
			return $returnValue;
		}
		
		public function getAllVillages($_user_id){
			$returnValue=false;
				$query='SELECT name, village_id FROM villages WHERE user = :user';
			try {
				$pdoCore = Core::getInstance();
				$pdoObject = $pdoCore->dbh->prepare($query);
				$queryArray = array(':user'=>$_user_id);
				
				if ($pdoObject->execute($queryArray)) {
					$pdoObject->setFetchMode(PDO::FETCH_ASSOC);;
					while ($listDataRow = $pdoObject->fetch()) {
						$this->name[]=$listDataRow['name'];
						$this->village_id[]=$listDataRow['village_id'];
					}
					$returnValue=true;
				}
			}
			catch(PDOException $pe) {
				trigger_error('Could not connect to MySQL database. ' . $pe->getMessage() , E_USER_ERROR);
			}
			return $returnValue;
		}
	}

	function workCards(){
		$sql = "SELECT * FROM work WHERE village = :id";
		try {
			$core = Core::getInstance();
			$stmt = $core->dbh->prepare($sql);
			$stmt->bindParam(':id', $this->id, PDO::PARAM_INT);
		 
			$result = $stmt->fetchAll();
			if ( count($result) ) { 
				foreach($result as $row) {
				echo '
					<div class="village-work-card">
						<h3>'.$row['name'].'</h3>
						<ul>
							<li><span class="definition">Workers</span><span class="defined">'.number_format($row['workers'], 0, ',', ' ').'</span></li>
							<li><span class="definition">Food</span><span class="defined">'.number_format($row['resources'], 0, ',', ' ').'</span></li>
							<li><span class="definition">Money</span><span class="defined">'.number_format($row['money'], 0, ',', ' ').'</span></li>
						</ul>
						<div class="card-details">
							<p>
								Apple Farm started up as a small farm up north, in the early 70s. Since then, it has found its success in growing the finest apples in Lumber Village!
							</p>
							<p>
								Apple Farm gathers about 14 food per day, however, their storage limit is 500. Their general mood is quite good standing, but the taxes could be lower!
							</p>
							<p><span class="tax-amount"></span></p>
							<div class="tax-slider"></div>
							<br>
							<div class="card-skill-container">
								<a href="#"><div class="skill-box">5</div></a>
								<a href="#"><div class="skill-box">1</div></a>
								<a href="#"><div class="skill-box">6</div></a>
								<a href="#"><div class="skill-box">10</div></a>
								<a href="#"><div class="skill-box">13</div></a>
								<a href="#"><div class="skill-box">4</div></a>
								<a href="#"><div class="skill-box">16</div></a>
								<div class="clear-both"></div>
							</div>
							<br>
						</div>
					</div>
				';
				}
			} else {
				
			}
		} catch(PDOException $e) {
			echo 'ERROR: ' . $e->getMessage();
		}
	}

//Check if user is logged in
	if(isset($_SESSION['login_code'])
			&& isset($_SESSION['user_id']))
	{
		$user = new User();
		$user->getUserBy('user_id', $_SESSION['user_id']);
		$isLogin = true;
		
		//Check if login_code is the same as the one stored in database
		if($user->data['login_code'] != $_SESSION['login_code']){
			session_destroy();
			header('location: /');
		}
	}else{
		$isLogin = false;
	}

//Login only
if($isLogin){
	
	/*Within this if statement, most of the forms are handled and are
	 * identified by the value of $_POST['action']
	 */
	if(isset($_POST['action'])){
		$action = htmlentities($_POST['action']);
		$error = '';
		
		//Introduction
		if($action == 'introduce' && $user->data['stage'] == 0){
			$introUsername = $_POST['username'];
			$introUsername = trim($introUsername);
			$introUsername = preg_replace("/[^a-zA-Z 0-9]+/", "", $introUsername);
			$introUsername = str_replace(' ', '', $introUsername);
			$introUsername = strip_tags($introUsername);
			$introUsername = htmlentities($introUsername);
			
			$introEmail = $_POST['email'];
			$introEmail = trim($introEmail);
			$introEmail = strip_tags($introEmail);
			$introEmail = htmlentities($introEmail);
			
			if(!empty($introEmail) && !empty($introUsername)){
				if(!filter_var($introEmail, FILTER_VALIDATE_EMAIL)){
					$error .= 'the email is incorrect.';
				}else{
					$user->updateUsernameAndEmail($introUsername, $introEmail, $user->data['user_id'], $user->data['stage']);
				}
			}else{
				$error .= 'username and email are required.';
			}
		}
		
		//First village
		if($action == 'first_village' && $user->data['stage'] == 1){
			$first_village_name = $_POST['name'];
			$first_village_name = preg_replace("/[^a-zA-Z 0-9]+/", " ", $first_village_name);
			$first_village_name = strip_tags($first_village_name);
			$first_village_name = htmlentities($first_village_name);
			$first_village_name = trim($first_village_name);
			
			$first_village_description = trim(htmlentities(strip_tags($_POST['description'])));
			$first_village_x = trim(htmlentities(strip_tags(preg_replace("/[^0-9]+/", "", $_POST['x']))));
			$first_village_y = trim(htmlentities(strip_tags(preg_replace("/[^0-9]+/", "", $_POST['y']))));
			
			if(!empty($first_village_name) && !empty($first_village_description)){
				$village = new Village();
				$village->newVillage($user->data['user_id'], $first_village_name, $first_village_description, $first_village_x, $first_village_y, $user->data['stage']);
			}else{
				$error .= 'your village needs a name and a description!';
			}
		}
		
	}
	
	//Logout
	if($_GET['logout'] == 'true'){
		session_destroy();
		header('location: /');
	}
	
	
	//Village data
	if(isset($_POST['updateVillageByID']) && isset($_POST['ajaxSelectedUser'])){
		$village = new Village();
		$input = trim(htmlentities(strip_tags($_POST['updateVillageByID'])));
		$village->getVillageBy('id', $input, $_POST['ajaxSelectedUser']);
		
		echo json_encode($village->data);
	}
	
	//Update village list
	if(isset($_POST['updateList'])){
		$villageList = new Village();
		$villageList->getAllVillages($user->data['user_id']);
		echo json_encode($villageList);
	}
}

//Inspect URL
if (isset($_GET['username'])){
	$urlUsername = str_replace('-', ' ', trim(htmlentities(strip_tags($_GET['username']))));
	
	$selectedUser = new User();
	$selectedUser->getUserBy('username', $urlUsername);
	
	if (isset($_GET['village'])){
		$urlVillage = str_replace('-', ' ', trim(htmlentities(strip_tags($_GET['village']))));
		
		$selectedVillage = new Village();
		$selectedVillage->getVillageBy('name', $urlVillage, $selectedUser->data['user_id']);
		
		/*Work has yet to be developed
		 * if (isset($_GET['work'])){
			$urlWork = str_replace('-', ' ', trim(htmlentities(strip_tags($_GET['work']))));
		}
		 */
	}
}
?>