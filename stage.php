<!DOCTYPE>
</html>
	<head>
		<meta charset="utf-8"/>
		<title>The Browser Project</title>
		<link rel="stylesheet" href="css/style-stage.css?ts=<?php echo time(); ?>"/>
		<!--[if ie]><meta http-equiv="X-UA-Compatible" content="IE=edge"/><![endif]-->
	</head>
	<body>
		<div id="container">
		<?php
			$gravemail = md5( strtolower( trim( $user->data['email'] ) ) );
			$gravsrc = "http://www.gravatar.com/avatar/".$gravemail;
			$gravcheck = "http://www.gravatar.com/avatar/".$gravemail."?d=404";
			$response = get_headers($gravcheck);
			if ($response[0] != "HTTP/1.0 404 Not Found"){
				$gravatar_avatar = '<img class="gravatar" src="'.$gravsrc.'">';
			}
		?>
		<?php if($user->data['stage'] == 0){ ?>
		<?php echo $gravatar_avatar; ?>
		<h1>Hi there</h1>
		<p><strong>Before you start, why don't you introduce yourself?</strong></p>
		<form action="/" method="POST">
			<input type="hidden" name="action" value="introduce">
			My username is <input maxlength="20" name="username" type="text"> and my email is <input type="email" name="email" value="<?php echo $user->data['email']; ?>" maxlength="255">.
			<?php if($error != ''){ echo '<p class="error">Whoops, '.$error.'</p>'; } ?>
			<p class="description">
				You privacy is respected, which is why you do not need to share your passwords, and your email will not be sold or used for spam.
				Don't worry about your username, you can change it later. Just make sure it does't include any spaces or special characters.
			</p>
			<br>
			<input type="submit" value="Introduce yourself">
		</form>
		<?php }else if($user->data['stage'] == 1){ ?>
		<?php echo $gravatar_avatar; ?>
		<h1>Are you feeling creative, <?php echo $user->data['username']; ?>?</h1>
		<p><strong>You better be, because you're about to imagine your own village! Tell me about it!</strong></p>
		<form action="" method="POST">
			<input type="hidden" name="action" value="first_village">
			My imaginary village's name is <input type="text" name="name">.
			<p><strong>What's the history of this picturesque village? Be creative!</strong></p>
			<textarea name="description"></textarea>
			<?php if($error != ''){ echo '<p class="error">Whoops, '.$error.'</p>'; } ?>
			<p class="description">
				Villages are so much more than a clustered network of some filthy buildings. Alone, a village may seem trivial, but when they come together, they can do the most
				astonishing things. Collaboration among villages and other owners, may be the most significant step to make a good society.
			</p>
			<p class="description">
				Keep in mind, being the owner is not about abusing your power. Sometimes you need to sacrifice important values in order to make a good welfare for the villagers.
			</p>
			<br>
			<input type="submit" value="Realize your village!">
		</form>
		<?php } ?>
		</div>
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
		<script src="js/jquery.sidr.min.js"></script>
		<script src="js/stage-script.js"></script>
	</body>
</html>