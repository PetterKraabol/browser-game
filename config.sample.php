<?php

	//Database
	Config::write('db.host', 'localhost');
	Config::write('db.port', '3306');
	Config::write('db.basename', 'DBNAME');
	Config::write('db.user', 'USERNAME');
	Config::write('db.password', 'PASSWORD');
	
	//Oauth
	Config::write('google.oauth.client.id', 'CLIENT-ID');
	Config::write('google.oauth.client.secret', 'CLIENT-SECRET');
	Config::write('google.oauth.redirect-uri', 'YOUR-DOMAIN/oauthlogin/google.php');
	

	class Config {
		static $confArray;
		public static function read($name) {
			return self::$confArray[$name];
		}
		public static function write($name, $value) {
			self::$confArray[$name] = $value;
		}
	}
?>