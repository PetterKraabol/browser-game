$(document).ready(function () {
	
	//allowUpdates tells if the system is allowed to update or not.
	var allowUpdate = false;
	
	//Get data from the user's default village.
	updateVillageData(currentVillageID);
	
	//Update village data and list every x ms
	setInterval(function(){
		if(allowUpdate){
			updateVillageData(currentVillageID);
		}
	}, 5000);
	
	//Do them cool animations
	$('#experience-bar-progress').animate({width: 600}, {duration: 3000});
	$('.statistics-container').animate({height: 0}, {duration: 2000});
	
	//Classes!
	$('.card-details').addClass('hide');
	
	//Making buttons and links interactive and useful
	
	$('#logo').draggable(); //$('#logo').draggable().click(function(){updateVillageData();});
	
	//Toggle village menu
	var menuIsOpen = false;
	$('li.menu-villages').sidr().click(function(){
		if(!menuIsOpen){
			$("#village-search-filter").focus().select();
			menuIsOpen = true;
		}else{
			$("#village-search-filter").blur();
			menuIsOpen = false;
		}
	});
	
	//Tax slider for village cards
	$('.tax-slider').slider({
	  range: "min",
	  value: 25,
	  min: 0,
	  max: 100,
	  slide: function( event, ui ) {
		$( ".tax-amount" ).html("Tax "+ ui.value +" %");
	  }
	});
	$( ".tax-amount" ).html("Tax "+$( ".tax-slider" ).slider( "value" )+" %");

	//Village card expand and collapse
	$(".village-work-card").click(function(){
		if($('.card-details',this).hasClass('hide')) {
			$('.card-details',this).animate({height:250},500).removeClass('hide');
		} else { 
			$('.card-details',this).animate({height:0},500).addClass('hide');
		}
	});
	
	//<--Testing
	$('.awesomebutton').click(function(){
		window.history.pushState(null, "Title", "/Zarlach");
		alert(document.location.href.replace(/^.*\/\/[^\/]+/, ''));
	});
	
	// Testing -> 
	
	//Update data for selected village
	function updateVillageData(inputID, doAction){
		allowUpdate = false;
		if(typeof(inputID)==='undefined') inputID = 'default';
		
		//If selecting a new village, slide!
		if(doAction === 'slide'){
			slideContent();
		}
		
		//Request some fresh data from process.php
		var request = $.ajax({
			type: 'POST',
			url: '/process.php',
			data: {updateVillageByID : inputID, ajaxSelectedUser : selectedUser},
			
			dataType: 'json'
		});
		request.done(function(data) {
			//Set village ID
			currentVillageID = data['village_id'];
			
			//If village_id returns empty, the user is most likely logged out. Refresh the page to check
			if(data['village_id'] === ''){
				document.location.reload(true);
			}
			
			//Update
			//Stats
			$('.village-name').html(data['name']);
			$('.village-coins').html(data['coins'].replace(/\B(?=(\d{3})+(?!\d))/g, " "));
			$('.village-food').html(data['food'].replace(/\B(?=(\d{3})+(?!\d))/g, " "));
			$('.village-wood').html(data['wood'].replace(/\B(?=(\d{3})+(?!\d))/g, " "));
			$('.village-stone').html(data['stone'].replace(/\B(?=(\d{3})+(?!\d))/g, " "));
			$('.village-health').html(data['health'].replace(/\B(?=(\d{3})+(?!\d))/g, " "));
			
			//Other
			$('.village-description').html(data['description']);

			//If user is busy filtering villages, do not disturb!
			if ($('#village-search-filter').val() === ''){
				updateVillageList();
			}
			allowUpdate = true;
		});
		
		//Couldn't get the village info? Oh noes...
		request.fail(function(jqXHR, textStatus) {
			document.location.reload(true);
			allowUpdate = true;
		});
	}
	
	//Update the village list
	function updateVillageList(){
		allowUpdate = false;
		var request = $.ajax({
			type: 'POST',
			url: '/process.php',
			data: {updateList : 'update'},
			
			dataType: 'json'
		});
		request.done(function(data) {
			//Clean the menu and update
			$('.village-list').html('');
			var counter = 0;
			$.each(data.village_id, function() {
				if(data.village_id[counter] !== currentVillageID){
					$('.village-list').append('<li><a href="#" class="'+data.village_id[counter]+'">'+data.name[counter]+'</a></li>');
				}
				counter++;
			});
			
			//When a user selects a village from the list
			$('ul.village-list li a').click(function(){
				updateVillageData($(this).attr('class'), 'slide');
				updateVillageList();
				$('#village-search-filter').focus().select();
			});
			allowUpdate = true;
		});
		 
		request.fail(function(jqXHR, textStatus) {
			document.location.reload(true);
			//alert( "Village request failed: " + textStatus );
			allowUpdate = true;
		});
	}
	
	//Slides the container off to the side and comes back in from the other
	function slideContent(){
		$('.container:last').animate({
			opacity: 0,
			marginLeft: '+=500'
		}, 500, function() {
			$(this).css('marginLeft', 'auto').css('marginRight', '+=500');
			$(this).animate({
				marginRight: '-=500',
				opacity:1
			}, 500, function(){
				$(this).css('marginRight', 'auto');
			});
		});
	}

	//Sidebar search filter
	$("#village-search-filter").keyup(function(){
		var filter = $(this).val(), count = 0;
		$(".village-list li").each(function(){
			if ($(this).text().search(new RegExp(filter, "i")) < 0) {
				$(this).fadeOut();
			} else {
				$(this).show();
				count++;
			}
		});
	});
	
	//Village description
	$('.toggle-village-description').click(function(){
		if($(this).hasClass('active')){
			$(this).text('Description');
			$(".village-description-container").height('0');
		}else{
			$(this).text('Collapse');
			$(".village-description-container").height($(".village-description").height());
		}
		$(this).toggleClass('active');
		$('.village-description-container').toggleClass('active');
	});
});