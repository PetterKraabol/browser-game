$(document).ready(function () {
	$('#container').animate({opacity: 1}, {duration: 1000});
	$('input:first').focus();
	
	//Submit
	$('form').submit(function(){
		$('#container').animate({opacity: 0}, {duration: 500});
	});
});