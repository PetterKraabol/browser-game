		<div id="top">
			<div class="container">
				<div id="logo"><a href="/"><h1>some<span>logo</span></h1></a></div>
				<nav>
					<ul id="menu-main">
						<li class="menu-villages clickable">
							<strong>Villages</strong>
							<span>toggle menu</span>
						</li>
						<li class="menu-map clickable">
							<strong>Map</strong>
							<span>world map</span>
						</li>
						<li class="menu-statistics clickable">
							<strong>Statistics</strong>
							<span>graphs and numbers</span>
						</li>
						<li class="menu-community clickable">
							<strong>Community</strong>
							<span>browse the forums</span>
						</li>
						<li>
							<a class="menu-community" href="?logout=true">
								<strong>Sign out</strong>
								<span>good bye</span>
							</a>
						</li>
					</ul>
				</nav>
				<!--<a href="" id="profile-icon"><img height="50" width="50" src="https://s3.amazonaws.com/empireavenue-public/portraits/p_lg_ZARLACH_5b9caf9ad28e.jpg"/></a>-->
			</div>
			<div class="clear-both"></div>
		</div>